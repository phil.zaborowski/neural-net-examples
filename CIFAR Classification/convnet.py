import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as functional


class CNN(nn.Module):
    def __init__(self, im_size, hidden_dim, kernel_size, n_classes):
        '''
        Create components of a CNN classifier and initialize their weights.

        Arguments:
            im_size (tuple): A tuple of ints with (channels, height, width)
            hidden_dim (int): Number of hidden activations to use
            kernel_size (int): Width and height of (square) convolution filters
            n_classes (int): Number of classes to score
        '''
        super(CNN, self).__init__()
        #############################################################################
        # Initialize what's needed for the forward pass
        #############################################################################
        channels, height, width = im_size
        self.conv = nn.Conv2d(channels, hidden_dim, kernel_size=kernel_size)
        self.fc = nn.Linear(int(hidden_dim * (height - kernel_size + 1) / 2 * (width - kernel_size + 1) / 2), n_classes)
        #############################################################################

    def forward(self, images):
        '''
        Take a batch of images and run them through the CNN to
        produce a score for each class.

        Arguments:
            images (Variable): A tensor of size (N, C, H, W) where
                N is the batch size
                C is the number of channels
                H is the image height
                W is the image width

        Returns:
            A torch Variable of size (N, n_classes) specifying the score
            for each example and category.
        '''
        scores = None
        #############################################################################
        # Implements the forward pass.
        #############################################################################
        images = functional.max_pool2d(functional.relu(self.conv(images)), (2, 2))
        images = images.view(-1, self.dimen(images))
        #print (self.dimen(images))
        scores = self.fc(images)
        #############################################################################

        return scores

    def dimen(self, x):
        dim_size = x.size()[1:]
        feat = 1
        for s in dim_size:
            feat *= s
        return feat
