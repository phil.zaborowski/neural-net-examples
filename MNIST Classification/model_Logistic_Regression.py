import torch
import torch.nn as nn
import torch.utils.data as data
import pandas as pd
import numpy as np

#imports train & test csv files
train = pd.read_csv('train.csv')
test = pd.read_csv('test.csv')
#Converts dataframes to tensors
mnist_trainset = torch.Tensor(train.values)
mnist_testset = torch.Tensor(test.values)

# Hyper Parameters
input_size,num_classes,num_epochs,batch_size,learning_rate = 784, 10, 5, 2, 0.01
train_loader = data.DataLoader(dataset=mnist_trainset,batch_size=batch_size,shuffle=True)
test_loader = data.DataLoader(dataset=mnist_testset,batch_size=batch_size,shuffle=False)

# Model
class LogisticRegression(nn.Module):
    def __init__(self, input_size, num_classes):
        super(LogisticRegression, self).__init__()
        self.linear = nn.Linear(input_size, num_classes)

    def forward(self, x):
        out = self.linear(x)
        return out

model = LogisticRegression(input_size, num_classes)

criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)

# Training the Model
for epoch in range(num_epochs):
    for i, (images, labels) in enumerate(train_loader):
        images = data[1:]
        labels = data[0]
        images = np.array(images, dtype='uint8')
        # Reshape the array into 28 x 28 array
        images = images.reshape((28, 28))

        # Forward + Backward + Optimize
        optimizer.zero_grad()
        outputs = model(images)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        if (i + 1) % 100 == 0:
            print('Epoch: [%d/%d], Step: [%d/%d], Loss: %.4f'
                  % (epoch + 1, num_epochs, i + 1, len(mnist_trainset) // batch_size, loss.data[0]))
# Test the Model
correct = 0
total = 0
for images, labels in test_loader:
    images = data[1:]
    images = np.array(images, dtype='uint8')
    # Reshape the array into 28 x 28 array
    images = images.reshape((28, 28))
    outputs = model(images)
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()
print('Accuracy of the model on the 28000 test images: %d %%' % (100 * correct / total))