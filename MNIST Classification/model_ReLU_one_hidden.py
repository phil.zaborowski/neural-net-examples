#Python 3.7
import pandas as pd
import numpy as np
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms

train_data = pd.read_csv('train.csv')
train_x = train_data
train_x = train_x.drop(train_x.columns[0], axis=1)
train_y = train_data[['label']]

N, D_in, H, D_out = 42000, 784, 100, 10

# Input and output tensors
x = torch.Tensor(train_x.values)
x = x.float() # conversion of numeric type
y = torch.Tensor(train_y.values)
y = y.view(y.numel()) # Needed to convert the [batch_size X 1] dimensional 2-D tensor into a 1-D tensor for size = batch_size

#Define the model
model = torch.nn.Sequential(
    torch.nn.Linear(D_in, H),
    torch.nn.ReLU(),
    torch.nn.Linear(H, D_out),
)

loss_fn = torch.nn.CrossEntropyLoss()

learning_rate = 1e-4
optimizer = torch.optim.Adam(model.parameters(), lr = learning_rate)
print(x.shape)
print(y.shape)

for t in range(100):
    y_pred = model(x)
    loss = loss_fn(y_pred, y)
    print(t, loss.item())
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

