#Python 3.7
import pandas as pd
import numpy as np
# Load Training set into dataframe
fullset = pd.read_csv('train.csv')
# Extract labels & transpose into an array
labels = np.asarray(fullset['label'])
# Put labels into bins and ouput distribution
print('Digits:  0 1 2 3 4 5 6 7 8 9')
print('labels: %s' % np.unique(labels))
print('Class distribution: %s' % np.bincount(labels))
'''
Output:
Digits:  0 1 2 3 4 5 6 7 8 9
labels: [0 1 2 3 4 5 6 7 8 9]
Class distribution: [4132 4684 4177 4351 4072 3795 4137 4401 4063 4188]
'''