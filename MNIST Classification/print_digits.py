#Python 3.7
import torch
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#imports train & test csv files
train = pd.read_csv('train.csv')
test = pd.read_csv('test.csv')
#Converts dataframes to tensors
mnist_trainset = torch.Tensor(train.values)
mnist_testset = torch.Tensor(test.values)
#Sets counter for while loop
counter = 0
while counter < 10:
    for data in mnist_trainset:
        # The first column is the label
        label = data[0]
        # The rest of columns are the pixels
        #If statements within the while loop cycles through all 10 digits, and display an example; breaks after 9
        if label == counter :
            pixels = data[1:]
            pixels = np.array(pixels, dtype='uint8')
            # Reshape the array into 28 x 28 array
            pixels = pixels.reshape((28, 28))
            plt.title('Label is {label}'.format(label=label))
            plt.imshow(pixels, cmap='gray')
            plt.show()
            counter += 1
        elif label > counter :
            continue
        elif label == 10 :
            break